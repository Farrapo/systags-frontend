<?php $__env->startSection('conteudo'); ?>
    <!-- primeira tela-->

    <div class="container-fluid priTela">
        <!-- LOGO IMG -->
        <div class="row" style="margin-top: 120px">
            <div class="col-md-4"></div>
            <div class="center-block col-md-1">
                <a href="/"><img width="118" height="91" src="/img/logoimg.png"> </a>
            </div>
        <!-- barra de pesquisa -->
            <div class="center-block col-md-2" style="margin-top:45px; margin-left:10px;">
                <form action="/consulta">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="pesquisar">
                        <span class="input-group-btn">
                        <button class="btn btn-warning pesquisarB" type="submit">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- segunda tela-->

    <div class="container-fluid segTelaVeic">
        <div class="row seta"> <!-- seta do container -->
            <img src="/img/setaBranca.png">
        </div>
        <!-- titulo -->
        <div class="row">
            <p class="text-center tituloVeiculos">Cadastrar Veiculos</p>
        </div>
        <!-- imagens -->
        <div class="veiculosInput">
            <form>
                <div class="input-group ">
                    <div class="row veicInput">
                        <input type="text" class="form-control" placeholder="modelo">
                    </div>
                    <div class="row veicInput">
                        <input type="text" class="form-control" placeholder="placa">
                    </div>
                    <div class="row veicInput">
                        <input type="text" class="form-control" placeholder="ano">
                    </div>
                    <div class="row veicInput">
                        <div class="btnveiculos">
                            <button class="btn btn-warning pesquisarB" type="button">Salvar</button>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>