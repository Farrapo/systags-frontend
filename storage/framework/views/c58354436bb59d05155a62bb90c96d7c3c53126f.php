<?php $__env->startSection('conteudo'); ?>
    <!-- primeira tela-->

    <div class="container-fluid priTela">
        <!-- LOGO IMG -->
        <div class="row" style="margin-top: 120px">
            <div class="col-md-4"></div>
            <div class="center-block col-md-1">
                <a href="/"><img width="118" height="91" src="/img/logoimg.png"> </a>
            </div>
            <!-- barra de pesquisa -->
            <div class="center-block col-md-2" style="margin-top:45px; margin-left:10px;">
                <form action="/consulta">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="pesquisar">
                        <span class="input-group-btn">
                        <button class="btn btn-warning pesquisarB" type="submit">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- segunda tela-->

    <div class="container-fluid segTelaConsulta">
        <div class="row seta"> <!-- seta do container -->
            <img src="/img/seta.png">
        </div>
        <!-- titulo -->
        <div class="row">
            <p class="text-center titulo">NOME DA CARGA AQUI</p>
        </div>

        <div class="row">
            <div class="col-md-1">
                <p class="img">
                    <img src="/img/2.png">
                </p>
            </div>
            <div class="col-md-5 consultaEsp">
                <p class="consultaTxt">
                    nome do cliente
                </p>
                <p class="consultaTxt">
                    estado da carga
                </p>
            </div>
            <div class="col-md-4 mapa">
                <div style='overflow:hidden;height:200px;width:400px;'>
                    <div id='gmap_canvas' style='height:200px;width:400px;'></div>
                    <div>
                        <small><a href="http://embedgooglemaps.com">embed google map</a></small>
                    </div>
                    <div>
                        <small><a href="https://noleggioauto.zone/">nessun costo di annullamento</a></small>
                    </div>
                    <style>#gmap_canvas img {
                            max-width: none !important;
                            background: none !important
                        }</style>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">
            <p class="consultaTxtCarga">Sua carga está aqui</p>
            </div>
        </div>
    </div>
    <!-- terceira tela-->
    <div class="container-fluid tercTela">
        <div class="row">
            <div class="col-md-2 msgImage">
                <img src="/img/msg.png">
            </div>
            <div class="col-md-3 msgText">
                <p>sempre quando seu pedido for<br>
                    atualizado receba um e-mail</p>
            </div>
            <form>
                <div class="col-md-5 msgInput">
                    <input type="text" class="form-control msgInput1" placeholder="colque a tag do seu pacote aqui">
                    <input type="text" class="form-control msgInput2" placeholder="digite seu email aqui">
                </div>
                <div class="btnedit">
                    <button class="btn btn-warning pesquisarB" type="button">enviar</button>
                </div>
            </form>
        </div>
    </div>


    <script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>

    <script type='text/javascript'>function init_map() {
            var myOptions = {
                zoom: 13,
                center: new google.maps.LatLng(-32.03821521374113, -52.118137374682654),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
            marker = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng(-32.03821521374113, -52.118137374682654)
            });
            infowindow = new google.maps.InfoWindow({content: '<strong>sysTAG</strong><br>rio grande, brazil<br>'});
            google.maps.event.addListener(marker, 'click', function () {
                infowindow.open(map, marker);
            });
            infowindow.open(map, marker);
        }
        google.maps.event.addDomListener(window, 'load', init_map);</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>