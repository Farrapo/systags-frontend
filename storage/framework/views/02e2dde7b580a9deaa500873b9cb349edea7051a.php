

<?php $__env->startSection('conteudo'); ?>
    <!-- primeira tela-->

    <div class="container-fluid priTela">
        <!-- LOGO IMG -->
        <div class="row" style="margin-top: 120px">
            <div class="col-md-4"></div>
            <div class="center-block col-md-1">
                <a href="/"><img width="118" height="91" src="/img/logoimg.png"> </a>
            </div>
        <!-- barra de pesquisa -->
            <div class="center-block col-md-2" style="margin-top:45px; margin-left:10px;">
                <form action="/consulta">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="pesquisar" required>
                        <span class="input-group-btn">
                        <button class="btn btn-warning pesquisarB" type="submit">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- segunda tela-->

    <div class="container-fluid segTelaVeic">
        <div class="row seta"> <!-- seta do container -->
            <img src="/img/setaBranca.png">
        </div>
        <!-- titulo -->
        <div class="row">
            <p class="text-center tituloVeiculos">Cadastrar Veiculos</p>
        </div>
        <!-- imagens -->
        <div class="veiculosInput">
            <?php echo Form::open(['action' => 'RequisicoesController@cadastrarVeiculo']); ?>


                <div class="input-group ">
                    <!--<div class="row veicInput">
                        <input type="text" class="form-control" placeholder="modelo">
                    </div>-->
                    <div class="row veicInput">
                        <input id="placa" name="placa" type="text" class="form-control" placeholder="placa">
                    </div>
                    <div class="row veicInput">
                        <input id="largura" name="largura" type="text" class="form-control" placeholder="largura (em m)">
                    </div>
                    <div class="row veicInput">
                        <input id="altura" name="altura" type="text" class="form-control" placeholder="altura (em m)">
                    </div>
                    <div class="row veicInput">
                        <input id="comprimento" name="comprimento" type="text" class="form-control" placeholder="comprimento (em m)">
                    </div>
                    <div class="row veicInput">
                        <input id="capacidade" name="capacidade" type="text" class="form-control" placeholder="capacidade (em t)">
                    </div>
                    <div class="row veicInput">
                        <div class="btnveiculos">
                            <?php echo Form::submit("Salvar", ['class' => 'btn btn-warning pesquisarB']); ?>

                        </div>
                    </div><br />
            <?php echo Form::close(); ?>

        </div>
    </div>

    <script>
        placa = $("#placa").val();
        altura = $("#altura").val();
        largura = $("#largura").val();
        comprimento = $("#comprimento").val();

        var dados = [placa, altura, largura, comprimento];

        function submeter() {
            $.ajax({
                type: "POST",
                url: "http://localhost:9000/veiculos/store",
                data: dados
            });
        }
    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.template', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>