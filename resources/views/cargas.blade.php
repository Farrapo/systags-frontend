@extends('layouts.template')

@section('conteudo')
    <!-- primeira tela-->

    <div class="container-fluid priTela">
        <!-- LOGO IMG -->
        <div class="row" style="margin-top: 120px">
            <div class="col-md-4"></div>
            <div class="center-block col-md-1">
                <a href="/"><img width="118" height="91" src="/img/logoimg.png"> </a>
            </div>
        <!-- barra de pesquisa -->
            <div class="center-block col-md-2" style="margin-top:45px; margin-left:10px;">
                <form action="/consulta">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="pesquisar" required>
                        <span class="input-group-btn">
                        <button class="btn btn-warning pesquisarB" type="submit" >
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- segunda tela-->

    <div class="container-fluid segTelaVeic">
        <div class="row seta"> <!-- seta do container -->
            <img src="/img/setaBranca.png">
        </div>
        <!-- titulo -->
        <div class="row">
            <p class="text-center tituloVeiculos">Cadastrar carga</p>
        </div>
        <!-- imagens -->
        <div class="veiculosInput">
            {!! Form::open(['action' => 'RequisicoesController@cadastrarCarga']) !!}

                <div class="input-group ">
                    <div class="row veicInput">
                        <input name="referencia" type="text" class="form-control" placeholder="tag">
                    </div>
                    <div class="row veicInput">
                        <input name="descricao" type="text" class="form-control" placeholder="nome da carga">
                    </div>
                    <div class="row veicInput">
                        <input name="cliente" type="text" class="form-control" placeholder="cliente">
                    </div>
                    <div class="row veicInput">
                        <input id="largura" name="largura" type="text" class="form-control" placeholder="largura (em m)">
                    </div>
                    <div class="row veicInput">
                        <input id="altura" name="altura" type="text" class="form-control" placeholder="altura (em m)">
                    </div>
                    <div class="row veicInput">
                        <input id="comprimento" name="comprimento" type="text" class="form-control" placeholder="comprimento (em m)">
                    </div>
                    <div class="row veicInput">
                        <input id="peso" name="peso" type="text" class="form-control" placeholder="peso (em kg)">
                    </div>
                    <div class="row veicInput">
                        <div class="btnveiculos">
                            {!! Form::submit("Salvar", ['class' => 'btn btn-warning pesquisarB']) !!}
                        </div>
                    </div><br />
                </div>

            {!! Form::close() !!}
        </div>
    </div>



@endsection