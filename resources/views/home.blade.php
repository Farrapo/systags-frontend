@extends('layouts.template')

@section('conteudo')
    <!-- primeira tela-->

    <div class="container-fluid priTela">
        <!-- LOGO IMG -->
        <div class="row">
            <div class="center-block col-md-2" style="float: none; padding-top:150px;">
                <a href="/"><img src="/img/logoimg.png"> </a>
            </div>
        </div>
        <!-- barra de pesquisa -->
        <div class="row">
            <div class="center-block col-md-3" style="float: none; padding-top: 60px; padding-bottom: 60px;">
                <form action="/consulta">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="pesquisar" required>
                        <span class="input-group-btn">
                        <button class="btn btn-warning pesquisarB" type="submit">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- segunda tela-->

    <div class="container-fluid segTela">
        <div class="row seta"> <!-- seta do container -->
            <img src="/img/seta.png">
        </div>
        <!-- titulo -->
        <div class="row">
            <p class="text-center titulo">sysTAG</p>
        </div>
        <!-- imagens -->
        <div class="imag">
            <p>
                <img class="imag-ali" src="/img/1.png">
                <img class="imag-ali" src="/img/2.png">
                <img class="imag-ali" src="/img/3.png">
            </p>
        </div>
        <!-- textos -->
        <div class="descr">
            <p>
                <b class="text-ali">cadastro</b>
                <b class="text-ali">atualização</b>
                <b class="text-ali">consulta</b>
            </p>
        </div>
    </div>
    <!-- terceira tela-->
    <div class="container-fluid tercTela">
        <div class="row">
            <div class="col-md-2 msgImage">
                <img src="/img/msg.png">
            </div>
            <div class="col-md-3 msgText">
                <p>sempre quando seu pedido for<br>
                atualizado receba um e-mail</p>
            </div>
            <form>
                <div class="col-md-5 msgInput">
                    <input type="text" class="form-control msgInput1" placeholder="colque a tag do seu pacote aqui">
                    <input type="text" class="form-control msgInput2" placeholder="digite seu email aqui">
                </div>
                <div class="btnedit">
                    <button class="btn btn-warning pesquisarB" type="button">enviar</button>
                </div>
            </form>
        </div>
    </div>




@endsection