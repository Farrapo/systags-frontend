<!doctype html>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>sysTAG</title>
    <meta name="description" content="Sistema de tag">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/systag.css">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>

</head>
<body>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="col-md-6">
            <div class="navbar-header">
                <div class="logo">
                    <a class="navbar-brand iconlogo" href="/">
                        <img alt="logo" src="/img/logo.png" width="20">
                    </a>
                    <a class="navbar-brand textlogo" href="/">
                        sysTAG
                    </a>

                    <!-- fazer o teste p/ ver se ta logado aqui -->
                    <div class="navbar-text navbar-left menu">
                        <a class="menu" href="/cadastrar/veiculos">
                            Veiculos
                        </a>
                        <a class="menu" href="/cadastrar/cargas">
                            Cargas
                        </a>
                        <a class="menu" href="/atualizar/cargas">
                            Atualizar carga
                        </a>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="navbar-text navbar-right menu">
                <a class="menu" href="/logar">
                    Logar
                </a>
            </div>
        </div>
    </div>
</nav>


@yield('conteudo')

<div class="container-fluid foot">
    <div class="row">
        <div class="col-md-8">
            <img class="logoP" src="/img/logoimg.png">
        </div>
        <div class="col-md-4">
            <p class="text-center footTxtTitulo">MENUS RÁPIDOS</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <p class="text-justify footTxtDesc">
                sysTAG é um sistema de acompanhamento de cargas, podendo as cargas serem consultadas,
                atualizadas, receber e-mail e sms com a atual situação dentre outras funcionalidades.
            </p>
        </div>
        <div class="col-md-6">
            <p class="text-center">
                <b class="chaves1">[</b>
                <b class="footSystag">SYSTAG</b>
                <b class="chaves2">]</b>
            </p>
        </div>
        <div class="col-md-3 text-left footMenu">
            <p>
                <a class="menuRap" href="#">
                    Cadastro de cargas
                </a>
            </p>
            <p>
                <a class="menuRap" href="#">
                    Cadastro de veiculos
                </a>
            </p>
            <p>
                <a class="menuRap" href="#">
                    Atualizar cargas
                </a>
            </p>
        </div>
    </div>
</div>
<div class="container-fluid text-center footer">
    <p style="margin-top: 5px; margin-bottom: 0px;">O uso deste site está sujeito aos termos e condições do Termo de Uso e Política de privacidade.
    <br>© sysTAG. Todos direitos reservados</p>
</div>


<!-- Scripts -->
<script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>