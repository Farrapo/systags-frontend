<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class RequisicoesController extends Controller
{
    public function cadastrarVeiculo(Request $request)
    {
	    $client = new Client([
		    // Base URI is used with relative requests
		    'base_uri' => 'http://localhost:9000/',
		    // You can set any number of default request options.
		    'timeout'  => 2.0,
	    ]);

	    $r = $client->request('POST', 'http://localhost:9000/veiculos', [
		    'form_params' => $request->all()
	    ]);

	    if ( $r )
	    {
			$msg = "Veículo cadastrado com sucesso";
			return redirect()->action('HomeController@index');
	    }
    }

    public function cadastrarCarga(Request $request)
    {
	    $client = new Client([
		    // Base URI is used with relative requests
		    'base_uri' => 'http://localhost:9000/',
		    // You can set any number of default request options.
		    'timeout'  => 2.0,
	    ]);

	    $r = $client->request('POST', 'http://localhost:9000/cargas', [
		    'form_params' => $request->all()
	    ]);


	    if ( $r )
	    {
		    return redirect()->action('RequisicoesController@pegarCarga', ['referencia' => $request->referencia]);
	    }
    }

    public function pegarCarga($referencia)
    {
	    $client = new Client([
		    // Base URI is used with relative requests
		    'base_uri' => 'http://localhost:9000/',
		    // You can set any number of default request options.
		    'timeout'  => 2.0,
	    ]);

	    $response = $client->request('GET', 'http://localhost:9000/cargas/'.$referencia);

	    $data = json_decode($response->getBody());

	    return view('consulta', ['carga' => $data]);

    }

    public function atualizarCarga(Request $request)
    {
	    $client = new Client([
		    // Base URI is used with relative requests
		    'base_uri' => 'http://localhost:9000/',
		    // You can set any number of default request options.
		    'timeout'  => 2.0,
	    ]);

	    $response = $client->request('PUT', 'http://localhost:9000/cargas/atualizar/'.$request->ref);

	    if($response)
	    {
		    return redirect()->action('RequisicoesController@pegarCarga', ['referencia' => $request->ref]);
	    }
    }
}
