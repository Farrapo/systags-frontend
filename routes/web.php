<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/cadastrar/veiculos', function () {
    return view('veiculos');
});

Route::get('/cadastrar/cargas', function () {
    return view('cargas');
});

Route::get('/atualizar/cargas', function () {
    return view('atualizar');
});

Route::get('/logar', function () {
    return view('login');
});

Route::get('/consulta/{referencia}', 'RequisicoesController@pegarCarga');


Auth::routes();

Route::get('/home', 'HomeController@index');

Route::post('/veiculos/cadastrarVeiculo', 'RequisicoesController@cadastrarVeiculo');

Route::post('/cargas/cadastrarCarga', 'RequisicoesController@cadastrarCarga');

Route::post('/cargas/atualizarCarga/', 'RequisicoesController@atualizarCarga');